# Symtris
Modulet er et lille tetris-inspireret drag-n-drop spil, udviklet til Rekontinens-kurset.

## Set up
Kør:
```npm install```og ```bower install```

## Konfiguration
Spillet kan konfigureres i src/api/settings.json. Her kan drag-elementer og drop-elementer defineres, samt udvalgte globale variabler.

## Build
Kør ```gulp build```


## Afrapportering
Hvis modulet skal afrapportere, skal denne kodestump tilføjes på alle masters. HUSK at erstatte VERSION_NUMBER og VÆGTNING
```javascript
if(!document.getElementById("autoLoader")){
    var my_script = document.createElement('script');
    my_script.setAttribute('id','autoLoader');
    document.head.appendChild(my_script);

    $.getScript('https://bb.githack.com/anhesr/api/raw/VERSION_NUMBER/StorylineInterface.js', function() {
      editScoring(VÆGTNING, ['symtrisScore']);
    });
}
```