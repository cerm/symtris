function playSound(e) {
    var e = new Howl({src: ["sounds/" + e + ".mp3"]});
    e.volume(.5), e.play()
}

function setupInterface() {
    interface = new AbstractInterface, console.log(interface), setupVariables()
}

function setupVariables() {
    if (console.log("setupVariables()"), interface.getVariable) {
        points = Number(interface.getVariable("symtrisScoreRaw")), $(".points").text("Points: " + Math.round(points) + "/" + displayPointsMax), console.log(points), 0 != points && (resume = !0);
        try {
            misses = JSON.parse(interface.getVariable("symtrisMisses"))
        } catch (e) {
            misses = []
        }
        console.log(misses), setupGame(!1)
    } else setTimeout(function () {
        setupVariables()
    }, 500)
    console.log('misses:' + JSON.stringify(misses, null, '\t'));
}

function setupComplete(e) {
    console.log("setupComplete()"), $(".symtris_container").append('<div class="complete_notice"></div>'), $(".complete_notice").append('<div class="complete_notice_bg"></div>'), $(".complete_notice").append('<div class="complete_notice_rating" ></div>'), $(".complete_notice_rating").append('<img class="rating_img"  src="">'), $(".complete_notice").append('<div id="stressPane" class="complete_notice_contentPane"></div>'), $("#stressPane").append('<img class="complete_notice_tabel" src="' + e.resultTabels[0].img + '">'), $("#stressPane").append('<div id="stressResultPane" class="complete_notice_resultPane"></div>'), $(".complete_notice").append('<div id="urgePane" class="complete_notice_contentPane"></div>'), $("#urgePane").append('<img class="complete_notice_tabel" src="' + e.resultTabels[1].img + '">'), $("#urgePane").append('<div id="urgeResultPane" class="complete_notice_resultPane"></div>'), $(".complete_notice").append('<div id="overPane" class="complete_notice_contentPane"></div>'), $("#overPane").append('<img class="complete_notice_tabel" src="' + e.resultTabels[2].img + '">'), $("#overPane").append('<div id="overResultPane" class="complete_notice_resultPane"></div>'), $(".complete_notice").append('<div id="falskPane" class="complete_notice_contentPane"></div>'), $("#falskPane").append('<img class="complete_notice_tabel" src="' + e.resultTabels[3].img + '">'), $("#falskPane").append('<div id="falskResultPane" class="complete_notice_resultPane"></div>'), $(".complete_notice").append('<button type="button" onclick="resetGame()" class="btn btn_reset">Prøv igen</button>'), e.dragables.forEach(function (e, t, n) {
        switch (e.group) {
            case"stress":
                $("#stressResultPane").append('<div id="' + e.id + '_result" style="top:' + e.top + '" class="complete_notice_result"></div>');
                break;
            case"urge":
                $("#urgeResultPane").append('<div id="' + e.id + '_result" style="top:' + e.top + '" class="complete_notice_result"></div>');
                break;
            case"over":
                $("#overResultPane").append('<div id="' + e.id + '_result" style="top:' + e.top + '" class="complete_notice_result"></div>');
                break;
            case"falsk":
                $("#falskResultPane").append('<div id="' + e.id + '_result" style="top:' + e.top + '" class="complete_notice_result"></div>')
        }
    }), resume ? (points >= threeStarRating ? $(".rating_img").attr("src", "images/3_star_rating.png") : points >= twoStarRating ? $(".rating_img").attr("src", "images/2_star_rating.png") : points >= oneStarRating && $(".rating_img").attr("src", "images/1_star_rating.png"), misses.forEach(function (e) {
        console.log(e), $("#" + e + "_result").css("background-image", "url(images/negative.png)")
    }), $("#progressbar").text("100% Gennemført")) : DEBUG ? $(".rating_img").attr("src", "images/2_star_rating.png") : ($(".complete_notice").hide(), $(".complete_notice_contentPane").hide())
    progress = 100;
}

function setupGame(e) {
    console.log("setupGame()"), $.getJSON("api/settings.json", function (t) {
        if (punishment = t.punishment, displayPointsMax = t.displayPointsMax, moduleMaxPoints = t.moduleMaxPoints, oneStarRating = t.oneStarRating, twoStarRating = t.twoStarRating, threeStarRating = t.threeStarRating, reporting = t.reporting, console.log(t), setupComplete(t), !e) {
            var n = t.dropables.length;
            $.each(t.dropables, function (e, t) {
                createDropElement(t, n, e)
            })
        }
        if (!resume) {
            numDragables = t.dragables.length, reward = displayPointsMax / numDragables;
            var i = t.dragables.sort(function (e, t) {
                return .5 - Math.random()
            });
            $.each(i, function (e, t) {
                if (showTutorial && 0 == e) {
                    console.info("showtutorial");
                    var n = createDragElement(t, numDragables, e, showTutorial);
                    console.info(n.id);
                    startTutorial($("#" + n.id))
                } else createDragElement(t, numDragables, e)
            })
        }
        $(".symtris_container").droppable(), setupGameOverWatcher()
    })
}

function setupGameOverWatcher() {
    console.log("setupGameOverWatcher()"), setInterval(function () {
        console.log('gameOver:' + gameOver);
        for (var e = $(".ui-draggable"), t = 0, n = 0; n < e.length; n++) e[n].tween.isActive() && (console.log("active"), t++);
        t >= GAMEOVER_LIMIT && (gameOver || showGameOver())
    }, 2e3)
}

function showGameOver() {
    console.log("showGameOver()"), TweenMax.killAll(), $(".tutorial").remove(), $(".symtris_container").append('<div id="gameover-container"><button type="button" onclick="tryAgain()" class="btn btn_game_over">OK, lad mig prøve igen</button></div>')
}

function tryAgain() {
    $(".ui-draggable").remove(), $("#gameover-container").remove(), resetGame()
}

function showSecondTutorialHint() {
    console.log("showSecondTutorialHint()"), $("#first-hint-container").hide(), secondHintShown || ($("#second-hint-container").show(), secondHintShown = !0)
}

function hideCompleteTutorial() {
    $("#first-hint-container").hide(), $("#second-hint-container").hide()
}

function hideSecondTutorialHint() {
    console.log("hideSecondTutorialHint()"), $("#second-hint-container").hide()
}

function hideFirstTutorialHint() {
    console.log("hideFirstTutorialHint()"), $("#first-hint-container").hide()
}

function showFirstTutorialHint(e) {
    firstHintShown || ($("#first-hint-container").show(), firstHintShown = !0)
}

function startTutorial(e) {
    pauseTweenOnTotaltime(e, 3 * SPEED_FACTOR, showFirstTutorialHint)
}

function pauseTweenOnTotaltime(e, t, n) {
    console.log("pauseTweenOnTotaltime()"), tutorialID = setTimeout(function () {
        try {
            e[0].tween.totalTime() >= t ? (e[0].tween.pause(), n(e)) : pauseTweenOnTotaltime(e, t, n)
        } catch (i) {
            pauseTweenOnTotaltime(e, t, n)
        }
    }, 100)
}

function resetGame() {
    showTutorial = !1, resume = !1, $("#progressbar").text("0% Gennemført"), $(".complete_notice").remove(), misses = [], points = 0, interface.setVariable && interface.setVariable("symtrisMisses", ""), updateProgression(), setupGame(!0)
    progress = 0;
}

function createDragElement(e, t, n, i) {
    var s = new Image;
    return s.id = e.id, s.src = e.img, s.targets = e.targets, s.attempts = 0, s.onload = function () {
        var e, i = parseInt($(".symtris_container").css("width")), a = parseInt($(".symtris_container").css("height")),
            o = this.width, r = this.height;
        if (0 == n) e = i / 2 - o / 2; else {
            e = i / 2 - o / 2 - (Math.random() - .5) * (.85 * i)
        }
        var c = 0 - r;
        this.style.position = "absolute", $("#" + this.id).css("left", e).css("top", c);
        var l = new TweenMax(s, 2 + t * SPEED_FACTOR - n, {css: {y: a + r / 2}, repeat: -1, ease: Power1.easeOut});
        l.eventCallback("onRepeat", function () {
            playSound("repeat")
        }), s.tween = l, 0 != n && s.tween.pause()
    }, 0 == n && i ? enableDragging(s, showSecondTutorialHint) : enableDragging(s), $(".symtris_container").append(s), s
}

function updateProgression(e) {
    if (console.log("updateProgression()"), $(".points").text("Points: " + Math.round(points) + "/" + displayPointsMax), interface.setVariable && (interface.setVariable("symtrisScore", points / displayPointsMax * moduleMaxPoints), interface.setVariable("symtrisScoreRaw", points)), console.log(e), void 0 !== e) {
        var t = Math.round(100 * (1 - e));
        progress = t;
        $("#progressbar").text(t + "% Gennemført")
    }
    console.log('progress:' + progress);
}

function addPointsToScore(e) {
    window.parent.addPoints && window.parent.addPoints(e)
}

function stopAnimation(e, t, n) {
    e.tween.pause(), e.style.transform, e.style.transform = n ? "none" : e.style.transform
}

function startAnimation(e, t) {
    var n = $("#" + e.id).height(), i = -1 * n;
    $("#" + e.id).css("top", i), e.style.transform = t, e.tween.resume()
}

function createDropElement(e, t, n) {
    var i = new Image;
    i.onload = function () {
        var i = parseInt($(".symtris_container").css("width")), s = parseInt($(".symtris_container").css("height")),
            a = this.width, o = this.height, r = (i - t * a) / (t * (t - 1)), c = i / t + r;
        this.style.position = "absolute", this.style.zIndex = -1;
        var l = c * n, p = s - o;
        $("#" + e.id).css("left", l).css("top", p)
    }, i.id = e.id, i.src = e.img, $(i).droppable({
        drop: function (e, t) {
            hideSecondTutorialHint();
            var n = !1, i = this.id;
            if ($.each(t.draggable[0].targets, function (e, t) {
                t.refName == i && (n = !0)
            }), n) {
                showTutorial = !1, points += reward;
                updateProgression(Number($("#" + t.draggable[0].id).siblings(".ui-draggable").length) / numDragables);
                try {
                    var s = $("#" + t.draggable[0].id).siblings(".ui-draggable")[0];
                    $("#" + s.id).show(), s.tween.play()
                } catch (e) {
                    setTimeout(function () {
                        playSound("completion"), interface.setVariable && (interface.setVariable("symtrisMisses", JSON.stringify(misses)), interface.setVariable("symtrisScoreRaw", Math.round(points))), window.parent.ReportStatus && reporting && window.parent.ReportStatus(), misses.forEach(function (e, t, n) {
                            console.log(e), $("#" + e + "_result").css("background-image", "url(images/negative.png)")
                        }), points >= threeStarRating ? $(".rating_img").attr("src", "images/3_star_rating.png") : points >= twoStarRating ? $(".rating_img").attr("src", "images/2_star_rating.png") : points >= oneStarRating && $(".rating_img").attr("src", "images/1_star_rating.png"), $(".complete_notice").show("bounce", 1e3), setTimeout(function () {
                            $(".complete_notice_contentPane").show("fade", 1e3)
                        }, 1e3)
                    }, 1500)
                }
                $("#" + t.draggable[0].id)[0].tween = null, $("#" + t.draggable[0].id).remove(), playSound("success")
            } else {
                t.draggable[0].attempts++, misses.indexOf(t.draggable[0].id) < 0 && (misses.push(t.draggable[0].id), interface.setVariable && interface.setVariable("symtrisMisses", JSON.stringify(misses))), console.log(misses);
                try {
                    for (var a = !1, o = 0; !a && o < 100;) {
                        var s = $("#" + t.draggable[0].id).siblings(".ui-draggable")[o];
                        $("#" + s.id)[0].tween.paused() ? ($("#" + s.id).show(), s.tween.play(), a = !0) : o++
                    }
                } catch (e) {
                }
                points >= punishment ? (points -= punishment, updateProgression()) : points > 0 && (points = 0, updateProgression()), playSound("fail")
            }
        }
    }), $(".symtris_container").append(i)
}

var progress = 0;
var points = 0, punishment = 2, displayPointsMax = 100, moduleMaxPoints = 25, reward, oneStarRating = 0,
    twoStarRating = 50, threeStarRating = 99, misses = [], resume = !1;
const DEBUG = !1;
var interface, reporting = !1, numDragables = 1e6, showTutorial = !0, firstHintShown = !1, secondHintShown = !1;
const SPEED_FACTOR = 2.5, GAMEOVER_LIMIT = 5;
var gameOver = !1, tutorialID;
$(document).ready(function () {
    $(".symtris_container").append('<div><div class="status progress" id="progressbar">0% Gennemført</div><div class="status points">Points: 0/100</div></div><div id="first-hint-container" class="tutorial"><div class="arrow"></div><div class="status" id="first-hint">Træk symptomet</div></div><div id="second-hint-container" class="tutorial"></div>'), $("#first-hint-container").hide(), $("#second-hint-container").hide(), setupInterface()
    window.addEventListener("unload", function (e) {
        if (progress < 100) {
            resetGame();
        }
        console.log('reporting:' + reporting);
        console.log('resume:' + resume);
    });

});
var enableDragging = function (e, t) {
    var n, i = !1;
    $(e).draggable({
        revert: "valid", start: function () {
            i = !0, stopAnimation(this, n, !0), t && (clearTimeout(tutorialID), t())
        }, stop: function () {
            i = !1, this.tween && startAnimation(this, n), t && hideCompleteTutorial()
        }
    }), $(e).hover(function () {
        stopAnimation(this, n, !1)
    }, function () {
        i || startAnimation(this, n)
    })
};