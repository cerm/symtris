/**
 * Created by Anders on 26/01/2017.
 */
var gulp = require('gulp'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    del = require('del'),
    htmlmin = require('gulp-htmlmin'),
    // htmlreplace = require('gulp-html-replace');
    gulpif = require('gulp-if'),
    useref = require('gulp-useref'),
    minifyCss = require('gulp-clean-css');

gulp.task('clean', function() {
    return del('dist/**/*');
});

gulp.task('html', function () {
    return gulp.src('src/*.html')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        // .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist'));
});

gulp.task('moveSettings', function () {
    return gulp.src('src/api/settings.json').pipe(gulp.dest('dist/api'));
});
gulp.task('moveImages', function () {
    return gulp.src('src/images/*').pipe(gulp.dest('dist/images'));
});
gulp.task('moveSounds', function () {
    return gulp.src('src/sounds/*').pipe(gulp.dest('dist/sounds'));
});

gulp.task('build', ['clean'], function() {
    gulp.start('html');
    gulp.start('moveSettings');
    gulp.start('moveImages');
    gulp.start('moveSounds');
});